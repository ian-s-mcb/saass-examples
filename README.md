# saass-examples

Jupyter Notebooks that demonstrate how to use [saass][saass] the image
segmentation library.

## Installation
```bash
pip install -r requirements-examples.txt
juptyer-notebook <example file>
```

[saass]: https://gitlab.com/ian-s-mcb/saass
